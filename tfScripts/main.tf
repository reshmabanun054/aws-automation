resource "aws_organizations_policy" "ctompolicy" {
  name ="ctompolicy"
  content = jsonencode(jsondecode(file("/builds/reshmabanun054/aws-automation/aws-scp/ctom-policy.json")))
}


resource "aws_organizations_policy_attachment" "ctompolicy_attach" {
  policy_id = aws_organizations_policy.ctompolicy.id
  target_id = "ou-z10r-t3g3v3tk"
}


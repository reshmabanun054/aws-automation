terraform {  
    backend "s3" {
        bucket  = "tf-st-files"
        key     = "Outputstate/terraform.tfstate"
    }
}
